#!/usr/bin/env python3

import unittest

from felix_fid import FIDstruct, FID


class TestFile(unittest.TestCase):
    def test_get_fid_tohost(self):
        self.assertEqual(0x123456709abc00f0, FID.get_fid(0x23, 0x4567, 0x09abc, 0xf0, False))

    def test_get_fid_tohost_hex(self):
        self.assertEqual('0x123456709abc00f0', hex(FID.get_fid(0x23, 0x4567, 0x09abc, 0xf0, False)).rstrip("L"))

    def test_get_fid_tohost_virtual_hex(self):
        self.assertEqual('0x123456789abc00f0', hex(FID.get_fid(0x23, 0x4567, 0x09abc, 0xf0, False, True)).rstrip("L"))

    def test_get_fid_fromhost_hex(self):
        self.assertEqual('0x123456709abc80f0', hex(FID.get_fid(0x23, 0x4567, 0x09abc, 0xf0, True)).rstrip("L"))

    def test_get_fid_fromhost_virtual_hex(self):
        self.assertEqual('0x123456789abc80f0', hex(FID.get_fid(0x23, 0x4567, 0x09abc, 0xf0, True, True)).rstrip("L"))

    def test_get_fid_tohost_22(self):
        self.assertEqual('0x1f00001000160000', hex(FID.get_fid(0xF0, 0x1, 22, 0x0, False)).rstrip("L"))

    def test_get_fid_fromhost_22(self):
        self.assertEqual('0x1f00001000168000', hex(FID.get_fid(0xF0, 0x1, 22, 0x0, True)).rstrip("L"))

    def test_elink_from_lid_and_tid(self):
        self.assertEqual('0x9abc', hex(FID.elink_from_lid_and_tid(0x026a, 0x3c80)).rstrip("L"))

    def test_elink_from_fid(self):
        self.assertEqual('0x9abc', hex(FID.elink_from_fid(0x123456789abc00f0)).rstrip("L"))

    def test_fid_definition(self):
        fid = FIDstruct()
        fid.asLong = 0x123456789abc80f0

        self.assertEqual(0x123456789abc80f0, fid.asLong)
        self.assertEqual(0xf0, fid.b.sid)
        self.assertEqual(0x3c80, fid.b.tid)
        self.assertEqual(0x226a, fid.b.lid)
        self.assertEqual(0x4567, fid.b.cid)
        self.assertEqual(0x23, fid.b.did)
        self.assertEqual(0x1, fid.b.vid)

        self.assertEqual(0x9abc, FID.elink_from_fid(0x123456789abc80f0))


if __name__ == '__main__':
    unittest.main()
