#pragma once

#include <stdint.h>
#include <stdio.h>

// version 0 is old-style elink as used in felixcore
#define FID_VERSION_ID 1
#define NO_OF_STREAMS 256

#if REGMAP_VERSION < 0x0500
// TTC-to-Host link = #Nchannels group #7 path #3 ---> (4) 0x13b, (12) 0x33b, (24) 0x63b
#define TTC2HOST_LINK(N) (((N) * 64) + (7 * 8) + 3)
#else
#define TTC2HOST_LINK(N) 0x600
#endif

#define STATS_LINK 0x1000
#define ERROR_LINK 0x1100
#define COMMAND_REPLY_LINK 0x1200
#define MON_LINK 0x1300

// Connector Offset (co) [general use] = Version Id (vid) + Detector Id (did) + Connector Id (cid) [atlas]
typedef union felix_co_1 {
  // NOTE: all types same width to avoid some warning about bitfields crossing width of types.
  uint32_t        co;        // Connector Offset
  struct __attribute__((__packed__)) {
    uint32_t       cid : 16;   // Connector Id
    uint32_t       did :  8;   // Detector Id
    uint32_t       vid :  4;   // Version Id
    uint32_t       na  :  4;   // Unused
  } b;
} felix_co_1_t;


typedef union felix_id_0 {
  uint64_t        fid;       // Felix Id
  // NOTE: no bitfields cross the types here.
  struct __attribute__((__packed__)) {
    uint32_t      elink: 32; // Elink
    uint32_t      co : 32;   // Connector Offset - always 0
  } b;
} felix_id_0_t;

typedef union felix_id_1 {
  uint64_t        fid;       // Felix Id
  // NOTE: all types same width to avoid some warning about bitfields crossing width of types.
  struct __attribute__((__packed__)) {
    uint64_t      sid :  8;  // Stream Id
    uint64_t      tid : 14;  // Transport Id, 6 bits used for elink, bit 7 set if is_fromhost
    uint64_t      lid : 14;  // Link Id
    uint64_t      cid : 16;  // Connector Id
    uint64_t      did :  8;  // Detector Id
    uint64_t      vid :  4;  // Version Id
  } b;
  struct __attribute__((__packed__)) {
    uint64_t      gid : 36;  // Generic ID
    uint64_t      co  : 28;  // Connector Offset
  } o;
  struct __attribute__((__packed__)) {
    uint64_t      xsid      : 8;  // Unused
    uint64_t      protocol  : 7;  // Protocol
    uint64_t      direction : 1;  // Virtual
    uint64_t      elink     : 19; // Elink
    uint64_t      vlink     : 1;  // Virtual
    uint64_t      xco       : 28; // Unused
  } e;
} felix_id_1_t;


inline uint32_t get_co_from_ids(unsigned did, unsigned cid, uint8_t vid) {
  switch(vid) {
    case 0:
      return 0;
    case 1:
    default: {
      felix_co_1_t connector_offset;
      connector_offset.b.vid = vid;
      connector_offset.b.did = did;
      connector_offset.b.cid = cid;
      connector_offset.b.na = 0;
      return connector_offset.co;
    }
  }
}

// NOTE: we need protocol here
inline uint64_t get_fid(uint32_t connector_offset, uint32_t elink, uint8_t stream_id, uint8_t vid, uint8_t is_fromhost, uint8_t is_virtual) {
  switch(vid) {
    case 0: {
      felix_id_0_t felix_id;
      felix_id.b.co = 0;  // always 0
      felix_id.b.elink = elink;
      return felix_id.fid;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.o.co = connector_offset;
      felix_id.b.vid = 1;
      felix_id.e.vlink = is_virtual;
      felix_id.e.elink = elink;
      felix_id.e.direction = is_fromhost;
      felix_id.e.protocol = 0;
      felix_id.b.sid = stream_id;
      return felix_id.fid;
    }
  }
}

inline uint64_t get_fid_from_ids(uint32_t did, uint32_t cid, uint32_t elink, uint8_t stream_id, uint8_t vid, uint8_t is_fromhost, uint8_t is_virtual) {
  uint32_t connector_offset = get_co_from_ids(did, cid, vid);
  return get_fid(connector_offset, elink, stream_id, vid, is_fromhost, is_virtual);
}

inline uint8_t get_vid(uint64_t fid) {
  felix_id_1_t felix_id;
  felix_id.fid = fid;
  return felix_id.b.vid;
}

inline uint32_t get_co(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.o.co;
    }
  }
}

inline uint32_t get_did(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.b.did;
    }
  }
}

inline uint32_t get_cid(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.b.cid;
    }
  }
}

inline uint32_t get_lid(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.b.lid;
    }
  }
}

inline uint32_t get_tid(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.b.tid;
    }
  }
}

inline uint32_t get_sid(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.b.sid;
    }
  }
}

inline uint8_t is_fromhost(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.e.direction;
    }
  }
}

inline uint8_t is_fromhost_from_tid(uint64_t tid) {
  felix_id_1_t felix_id;
  felix_id.b.tid = tid;
  return felix_id.e.direction;
}

inline uint8_t is_virtual(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.e.vlink;
    }
  }
}

inline uint8_t is_virtual_from_lid(uint64_t lid) {
  felix_id_1_t felix_id;
  felix_id.b.lid = lid;
  // NOTE: gcc 13.1.0 WRONGLY optimizes (-O2 and -O3) away this function and
  // then complains that felix_id.e.vlink is uninitialized.
  // To prevent this optimization a non-reachable print statement was added.
  if (lid > 20000) {
    printf("FIXME, remove this print statement and vlink becomes unitialized: %lx\n", felix_id.fid);
  }
  return felix_id.e.vlink;
}

inline uint32_t get_protocol(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.e.protocol;
    }
  }
}


inline uint32_t get_elink_from_ids(unsigned lid, unsigned tid, uint8_t vid) {
  felix_id_1_t felix_id;
  felix_id.b.tid = tid;
  felix_id.b.lid = lid;
  felix_id.b.vid = vid;
  return felix_id.e.elink;
}

inline uint32_t get_elink(uint64_t fid) {
  uint8_t vid = get_vid(fid);
  switch(vid) {
    case 0: {
      felix_id_0_t felix_id;
      felix_id.fid = fid;
      return felix_id.b.elink;
    }
    case 1:
    default: {
      felix_id_1_t felix_id;
      felix_id.fid = fid;
      return felix_id.e.elink;
    }
  }
}

inline uint32_t get_did_from_co(uint32_t co, uint8_t vid) {
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_co_1_t connector_offset;
      connector_offset.co = co;
      return connector_offset.b.did;
    }
  }
}

inline uint32_t get_cid_from_co(uint32_t co, uint8_t vid) {
  switch(vid) {
    case 0: {
      return 0;
    }
    case 1:
    default: {
      felix_co_1_t connector_offset;
      connector_offset.co = co;
      return connector_offset.b.cid;
    }
  }
}

// for backwards compatibility
inline uint8_t is_to_flx(uint64_t fid) {
  return is_fromhost(fid);
}

// for backwards compatibility
inline uint8_t is_to_flx_from_tid(uint64_t tid) {
  return is_fromhost_from_tid(tid);
}
