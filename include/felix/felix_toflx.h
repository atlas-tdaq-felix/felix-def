#pragma once

struct felix_toflx_header {
    uint32_t length;
    uint32_t reserved;
    uint64_t elink;
};
