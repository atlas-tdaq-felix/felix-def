#pragma once

#define PROTO_GBT 0
#define PROTO_FULL_MODE 1
#define PROTO_LPGBT_10_24 2
#define PROTO_LPGBT_5_12 3
#define PROTO_AURORA_64b66b 4
#define PROTO_INTERLAKEN_67b66b 5
#define PROTO_6b8b_LCB 6
#define PROTO_ENDEAVOUR 7
