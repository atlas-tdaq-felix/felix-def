#pragma once

struct ToFlxHeader {
    uint32_t length;
    uint32_t reserved;
    uint64_t elink;
};

